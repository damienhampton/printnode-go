package printnode

func (p *Printnode) WhoAmI() (*WhoAmI, error) {
	result := WhoAmI{}
	err := p.sendRequest("/whoami", &result)
	return &result, err
}

func (p *Printnode) Printers() (*[]Printer, error) {
	result := make([]Printer, 0)
	err := p.sendRequest("/printers", &result)
	return &result, err
}

func (p *Printnode) PrintJobs() (*[]PrintJob, error) {
	result := make([]PrintJob, 0)
	err := p.sendRequest("/printjobs", &result)
	return &result, err
}
