package printnode

type WhoAmI struct {
	APIKeys              []interface{} `json:"ApiKeys"`
	Tags                 []interface{} `json:"Tags"`
	CanCreateSubAccounts bool          `json:"canCreateSubAccounts"`
	ChildAccounts        []interface{} `json:"childAccounts"`
	Connected            []interface{} `json:"connected"`
	CreatorEmail         interface{}   `json:"creatorEmail"`
	CreatorRef           interface{}   `json:"creatorRef"`
	Credits              interface{}   `json:"credits"`
	Email                string        `json:"email"`
	Firstname            string        `json:"firstname"`
	ID                   int64         `json:"id"`
	Lastname             string        `json:"lastname"`
	NumComputers         int64         `json:"numComputers"`
	Permissions          []string      `json:"permissions"`
	State                string        `json:"state"`
	TotalPrints          int64         `json:"totalPrints"`
	Versions             []interface{} `json:"versions"`
}

type PrintJob struct {
	ContentType     ContentType `json:"contentType"`
	CreateTimestamp string      `json:"createTimestamp"`
	ExpireAt        string      `json:"expireAt"`
	ID              int64       `json:"id"`
	Printer         Printer     `json:"printer"`
	Source          Source      `json:"source"`
	State           State       `json:"state"`
	Title           Title       `json:"title"`
}

type Printer struct {
	Capabilities    Capabilities `json:"capabilities"`
	Computer        Computer     `json:"computer"`
	CreateTimestamp string       `json:"createTimestamp"`
	Default         bool         `json:"default"`
	Description     Description  `json:"description"`
	ID              int64        `json:"id"`
	Name            Description  `json:"name"`
	State           PrinterState `json:"state"`
}

type Capabilities struct {
	Bins                    []interface{} `json:"bins"`
	Collate                 bool          `json:"collate"`
	Color                   bool          `json:"color"`
	Copies                  int64         `json:"copies"`
	Dpis                    []interface{} `json:"dpis"`
	Duplex                  bool          `json:"duplex"`
	Extent                  interface{}   `json:"extent"`
	Medias                  []interface{} `json:"medias"`
	Nup                     []int64       `json:"nup"`
	Papers                  interface{}   `json:"papers"`
	Printrate               interface{}   `json:"printrate"`
	SupportsCustomPaperSize bool          `json:"supports_custom_paper_size"`
}

type Computer struct {
	CreateTimestamp string        `json:"createTimestamp"`
	Hostname        Hostname      `json:"hostname"`
	ID              int64         `json:"id"`
	Inet            Inet          `json:"inet"`
	Inet6           interface{}   `json:"inet6"`
	JRE             interface{}   `json:"jre"`
	Name            Name          `json:"name"`
	State           ComputerState `json:"state"`
	Version         Version       `json:"version"`
}

type ContentType string
type Hostname string
type Inet string
type Name string
type ComputerState string
type Version string
type Description string
type PrinterState string
type Source string
type State string
type Title string

type PrintnodeError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
	Uid     string `json:"uid"`
}
