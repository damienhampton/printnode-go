package printnode

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

type Printnode struct {
	apiKey     string
	HTTPClient *http.Client
	baseURL    string
}

func New(apiKey string) *Printnode {
	return &Printnode{
		apiKey: apiKey,
		HTTPClient: &http.Client{
			Timeout: 5 * time.Minute,
		},
		baseURL: "https://api.printnode.com",
	}
}

func (p *Printnode) sendRequest(url string, result interface{}) error {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s%s", p.baseURL, url), nil)
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "application/json; charset=utf-8")
	req.Header.Add("Authorization", "Basic "+basicAuth(p.apiKey, ""))

	res, err := p.HTTPClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	// Try to unmarshall into errorResponse
	if res.StatusCode != http.StatusOK {
		var errRes PrintnodeError
		if err = json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return errors.New(errRes.Message)
		}
		return fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	// Unmarshall and populate result
	if err = json.NewDecoder(res.Body).Decode(&result); err != nil {
		return err
	}
	return nil
}

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}
