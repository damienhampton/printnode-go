//go:build integration
// +build integration

package printnode_test

import (
	"github.com/joho/godotenv"
	"gitlab.com/damienhampton/printnode-go"
	"os"
	"testing"
)

func TestUnauthorized(t *testing.T) {
	printnode := printnode.New("")

	_, err := printnode.WhoAmI()

	if err.Error() != "API Key not found" {
		t.Error("Should return an unauthorized error!", err)
	}
}

func TestWhoAmI(t *testing.T) {
	printnode := printnode.New(os.Getenv("API_KEY"))

	whoami, err := printnode.WhoAmI()

	if err != nil {
		t.Error("Should not return an error!", err)
	}

	if len(whoami.Email) <= 0 {
		t.Error("Email length should be greater than zero")
	}
}

func TestPrinters(t *testing.T) {
	printnode := printnode.New(os.Getenv("API_KEY"))

	printers, err := printnode.Printers()

	if err != nil {
		t.Error("Should not return an error!", err)
	}

	if len(*printers) <= 0 {
		t.Error("There should be at least one printer")
	}

	if len((*printers)[0].Name) <= 0 {
		t.Error("Printer name length should be at greater than zero")
	}
}

func TestPrintJobs(t *testing.T) {
	printnode := printnode.New(os.Getenv("API_KEY"))

	printjobs, err := printnode.PrintJobs()

	if err != nil {
		t.Error("Should not return an error!", err)
	}

	if len(*printjobs) <= 0 {
		t.Error("There should be at least one print job")
	}

	if len((*printjobs)[0].Title) <= 0 {
		t.Error("Print job title length should be at greater than zero")
	}
}

func setup() {
	godotenv.Load()
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}
